#!/bin/bash

# Set color codes that will be used to print texts
defcol="\e[0m"
red="\e[1;31m"
magenta="\e[1;35m"
cyan="\e[1;36m"
bgblue="\e[44m"

echo -e "${magenta}POST CHROOT (You are now inside the partition where your system will be, right?)${defcol}"
echo -e "${red}Warning:${defcol} This script must be ran by ${bgblue}root${defcol}"
echo

echo -e "${cyan}I. Time zone, locale, and virtual console configuration${defcol}"
ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && echo Time zone was set!
echo pt_BR.UTF-8 UTF-8 >> /etc/locale.gen
echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
locale-gen > /dev/null
echo LANG=pt_BR.UTF-8 > /etc/locale.conf && echo Locale was set!
echo KEYMAP=br-abnt2 > /etc/vconsole.conf && echo Virtual console keymap was set!

echo -e "${cyan}II. Configure Pacman and install some essential packages${defcol}"
cp mirrorlist /etc/pacman.d/mirrorlist
sed -i "/ParallelDownloads/s/#//" /etc/pacman.conf
pacman -Syu --noconfirm grub intel-ucode networkmanager man-db man-pages texinfo nano neovim

echo -e "${cyan}III. Install and configure Grub${defcol}"
echo -n "What is the partition scheme in use, [M]BR or [G]PT? (Only enter initial letter): "
read -r partscheme
[[ $partscheme == "M" || $partscheme == "m" ]] &&
    echo -n "Enter the name of the device where Grub will be installed (as it appears in /dev/): " &&
    read -r device &&
    grub-install /dev/$device > /dev/null && echo Grub was installed successfully!
[[ $partscheme == "G" || $partscheme == "g" ]] &&
    echo -n "Enter which will be the name of this bootloader when showed in the boot menu list: " &&
    read -r id &&
    pacman -S --noconfirm efibootmgr && grub-install --efi-directory="/boot/efi" --bootloader-id=$id --target="x86_64-efi" > /dev/null && echo Grub was installed successfully!
grub-mkconfig -o /boot/grub/grub.cfg

echo -e "${cyan}IV. Enable internet on installed system${defcol}"
systemctl enable systemd-networkd NetworkManager > /dev/null

echo -e "${cyan}V. Name your system${defcol}"
echo -n "This is how it will appear to other devices: "
read -r hostname
echo $hostname > /etc/hostname

echo -e "${cyan}VI. Define root password${defcol}"
passwd

echo -e "${cyan}You can now reboot your system${defcol}"
