# myarchinstall

This repo contains some scripts that help me build my environment on Arch Linux easily, without having to do any complicated thing after formatting the disks, installing the 3 base packages, generating the file system table and chrooting into the desired root partition. Feel free to make a fork with your own preferences.

In order to use the scripts, you simply need to install Git after getting into the new root partition and clone the repo there.

The three scripts are:

1. postchroot - This one defines basic system settings, installs Grub, enables network support and sets the hostname, as well as the root password.
1. postinstall - This one simply creates your personal user, lets you choose its password, and adds it to important groups.
1. postuserbirth - (Must be executed inside the newly created user account) Used to install paru and all the programs i use. Also, imports [my dotfiles](https://gitlab.com/akamofu/dotfiles) and configures a few more things.

Obs.: Their names kinda tell when you should run each.

Then you should have almost the same setup as me in your machine.
