#!/bin/bash

# Set color codes that will be used to print texts
defcol="\e[0m"
red="\e[1;31m"
magenta="\e[1;35m"
cyan="\e[1;36m"
bgblue="\e[44m"

echo -e "${magenta}POST USER BIRTH (Have you switched to your personal user?)${defcol}"
echo -e "${red}Warning:${defcol} This script must be ran by ${bgblue}a non-root user${defcol}"
echo

echo -e "${cyan}I. Install Paru (AUR helper)${defcol}"
cd ~/Downloads/
git clone https://aur.archlinux.org/paru-bin.git
cd paru-bin/
makepkg -si

echo -e "${cyan}II. Install packages${defcol}"
# TODO: Add --noconfirm option (after making the necessary adjustments)
export GOPATH=$HOME/.go/
`## Audio` \
paru -S pulseaudio pulseaudio-alsa pulseaudio-jack pamixer pavucontrol pipewire pipewire-jack wireplumber \
`## Shell interface` \
bash-completion zsh zsh-completions \
`## Shell scripting/miscelaneous` \
neofetch htop tmux stow pass fzf yt-dlp \
`## Multimedia` \
mpv obs-studio spotify libreoffice-still firefox tor torbrowser-launcher \
`## Background programs` \
xremap qbittorrent \
`## Chatting` \
discord weechat \
`## Fonts` \
gnu-free-fonts ttf-font-awesome ttf-nerd-fonts-symbols ttf-dejavu ttf-liberation otf-fira-mono ttf-jetbrains-mono-nerd ttf-ubuntu-mono-nerd ttf-indic-otf wqy-zenhei noto-fonts noto-fonts-emoji noto-fonts-cjk noto-fonts-tc \
`## Themes and icons` \
qt5ct qt6ct gnome-themes-extra adwaita-qt6 kora-icon-theme qt6-multimedia-ffmpeg \
`## File permissions, support, sync and compression` \
polkit lxqt-policykit zip unzip unrar ntfs-3g usbutils android-file-transfer syncthing onedriver \
`## Programming` \
git lazygit base-devel asdf-vm \
`## Tex` \
texlive-latexrecommended texlive-latexextra texlive-plaingeneric \
`## GUI` \
gnome kitty loupe

echo -e "${cyan}III. Import dotfiles${defcol}"
cd ~
git clone https://gitlab.com/sadparadise/dotfiles .dotfiles
cd ~/.dotfiles/
stow --dotfiles dots

echo -e "${cyan}IV. Disable Pipewire${defcol}"
sudo systemctl --global disable pipewire.service
sudo systemctl --global disable wireplumber.service

echo -e "${cyan}V. Set zsh as the default shell for user${defcol}"
chsh -s /bin/zsh

echo -e "${cyan}VI. Create some folders${defcol}"
cd ~
mkdir Miscelânea
mkdir OneDrive

echo -e "${cyan}VII. Enable display manager${defcol}"
sudo systemctl enable gdm

echo -e "${cyan}Now reboot your machine. Your system is configured!${defcol}"
