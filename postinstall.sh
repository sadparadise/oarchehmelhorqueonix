#!/bin/bash

# Set color codes that will be used to print texts
defcol="\e[0m"
red="\e[1;31m"
magenta="\e[1;35m"
cyan="\e[1;36m"
bgblue="\e[44m"

echo -e "${magenta}POST INSTALL (This should be your first time booting the base system)${defcol}"
echo -e "${red}Warning:${defcol} This script must be ran by ${bgblue}root${defcol}"
echo

echo -e "${cyan}I. Install additional firmware, privilege escalation tools, base-devel and xdg-user-dirs${defcol}"
pacman -S --noconfirm sudo  xdg-user-dirs base-devel xf86-video-intel vulkan-intel mesa alsa-firmware alsa-utils sof-firmware

echo -e "${cyan}II. Give wheel group root privileges${defcol}"
cp sudoers /etc/sudoers

echo -e "${cyan}III. Create personal user${defcol}"
echo -n "Chose your personal user's name: "
read -r user
useradd -m -G wheel $user
usermod -aG audio $user
usermod -aG sys $user
usermod -aG input $user

echo -e "${cyan}IV. Define personal user's password${defcol}"
passwd $user

echo -e "${cyan}Now log into the user you've just created${defcol}"
